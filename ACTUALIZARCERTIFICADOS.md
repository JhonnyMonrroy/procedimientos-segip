# PROCEDIMIENTO PARA RENOVAR CERTIFICADOS EN LOS SERVIDORES

## Autenticarse en el servidor web
Para este fin se debe contar con un usuario que tenga las credenciales de acceso al servidor con privilegios de super usuario para poder crear archivos dentro del directorio /opt/ssl del Servidor Linux.

### Paso 1 - Iniciar sesión
Con una terminal de consola iniciar la sesión en el servidor a actualizar
```bash
ssh usuario@<IP-SERVIDOR>
```
### Paso 2 - Introducir la contraseña del usuario
>```bash
>usuario@<IP-SERVIDOR>´s password: 
>```
### Paso 3 - Verificar privilegios de Super usuario
```bash
sudo ls /opt/ssl/ -la
```
Ejemplo de salida
>```bash
>total 8
>drwxr-xr-x  2 root root 4096 may 13 14:15 .
>drwxr-xr-x 18 root root 4096 may 13 14:27 .. 
>```

## Verificar validez del certificado actual
En el host donde se quiere actualizar el certificado digital, buscar la configuración del servidor HTTP, especialmente la configuracion de los sitios que son accedidos por HTTPS.

Para este ejemplo se basó en las configuraciones de un servidor *"Nginx"*.

### Paso 1 - Ubicar la localización de los certificados a actualizar
Con un editor de texto abrir el archivo de configuración de algun sitio montado en el servidor nginx.

```bash
cat /etc/nginx/sites-enabled/<nombre_sitio>.conf
```
Ejemplo de salida.
>
>```conf
>server {
>    ...
>}
>server {
>    listen 443 ssl;
>    server_name <nombre_sitio>.segip.gob.bo;
>    
>    ...
>    ssl_certificate                 /opt/ssl/nginx.crt;
>    ssl_certificate_key             /opt/ssl/nginx.key;
>    ssl_protocols                   TLSv1 TLSv1.1 TLSv1.2;
>    ssl_prefer_server_ciphers       on;
>    ssl_ciphers                     'EECDH+AESGCM:EDH+AESGCM:AES256<+EECDH:AES256+EDH';
>    ssl_session_cache               shared:SSL:10m;ssl_session_timeout             10m;
>
>    ...
>    location / {
>        proxy_pass      http://10.0.4.196:57006;
>        include         /etc/nginx/proxy_params;
>    }
>}                               
>```

Prestar atención a las rutas de:

* ssl_certificate                 /opt/ssl/nginx.crt;
* ssl_certificate_key             /opt/ssl/nginx.key;

### Paso 2 - Ubicar la fecha de caducidad del Certificado Digital
Proceder a leer la información del certificado ya Existente nginx.crt
```bash
openssl x509 -in certExistente.crt -text -noout
```
Ejemplo de salida
>```bash
>Certificate:
>    Data:
>        Version: 3 (0x2)
>        Serial Number:
>            03:02:f0:22:ef:5a:fb:28:1d:8f:c7:e0:84:77:99:fa
>        Signature Algorithm: sha256WithRSAEncryption
>        Issuer: C = US, O = DigiCert Inc, OU = www.digicert.com, CN = GeoTrust RSA CA 2018
>        Validity
>            Not Before: Apr  7 00:00:00 2020 GMT
>            Not After : Jun  1 12:00:00 2022 GMT
>        Subject: C = BO, L = Santa Cruz de la Sierra, O = Servicio General de Identificacion Personal, OU = UNIDAD DE IMPLEMENTACION Y EXPLOTACION DE APLICACIONES, CN = *.segip.gob.bo
>        Subject Public Key Info:
>            Public Key Algorithm: rsaEncryption 
>                RSA Public-Key: (2048 bit)
>                Modulus:
>                    00:d7:db:af:0b:b9:b5:f9:08:b0:eb:21:25:d5:5a:
>                    55:af:a5:4d:8f:03:13:48:d8:ef:71:16:0d:2e:21:
>                    c3:cc:9c:4b:0b:78:d7:e5:85:49:ce:d8:38:54:d0:
>                    ...
>```

## Generar una solicitud de certificado CSR
En caso de que el certificado ya no esté vigente, se tiene que generar una solicitud de emisión de certificado digital renovado... para este fin se tiene 3 opciones:

### Generar un nuevo par de claves y a partir de esta generar un nuevo CSR. (RECOMENDADO)
#### Paso 1 - Generar una nueva clave privada

Si se desea generar una nueva clave privada y solicitud CSR, ejecute el siguiente comando.

```bash
openssl req -out Segip2022.csr -new -newkey rsa:2048 -nodes -keyout Segip2022.key
```
Ejemplo de salida en donde tendrá que completar los datos de la solicitud del certificado.

    Generating a RSA private key
    .................+++++
    ............................................................................................................................................................................................................................................................................................................................................................................+++++                                   writing new private key to 'nuevaClave.key'
    -----
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value, 
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]: 
    . . .

> **Nota.-** Esta clave privada deberá ser almacenada junto con el certificado que se generará posteriormente.

#### Paso 2 - Verificar la integridad del CSR generado
Se generó una nueva solicitud CSR, para verificar el mismo ejecute:
```bash
openssl req -text -noout -verify -in Segip2022.csr
```
Ejemplo de salida
>```bash
>verify OK 
>Certificate Request:
>    Data:
>        Version: 1 (0x0)
>        Subject: C = BO, L = Santa Cruz de la Sierra, O = Servicio General de Identificacion Personal, OU = UNIDAD DE IMPLEMENTACION Y EXPLOTACION DE APLICACIONES, CN = *.segip.gob.bo
>        Subject Public Key Info:
>            Public Key Algorithm: rsaEncryption
>                RSA Public-Key: (2048 bit)
>                Modulus:
>                    00:d7:db:af:0b:b9:b5:f9:08:b0:eb:21:25:d5:5a:
>                    55:af:a5:4d:8f:03:13:48:d8:ef:71:16:0d:2e:21:
>                    c3:cc:9c:4b:0b:78:d7:e5:85:49:ce:d8:38:54:d0:
>                    . . .
>```



### Generar un CSR con los datos base de un certificado ya existente y con su correspondiente llave privada para este certificado.
#### Paso 1 - Verificar la llave privada existente

```bash
openssl rsa -in /opt/ssl/nginx.key -check
```
Ejemplo de salida
>```bash
>RSA key ok 
>writing RSA key 
>-----BEGIN RSA PRIVATE KEY----- 
>MIIEpAIBAAKCAQEA19uvC7m1+Qiw6yEl1VpVr6VNjwMTSNjvcRYNLiHDzJxLC3jX
>5YVJztg4VNCrfPHMJm8a+Who56oJcarczTh6Q/SuTh62kaIUFviKbybWDnD0wLvj
>BVeGNXAeQyQaaD8x7vhquaNaAuwYcBCrJPWNLm4lz/BT5UEPi90+MZ8Rt314mFjR
> ...
>xvCEeem8gPUm5ASLq9z1vLQLu4W7wBa9h2vay2pcu3z9kRkOaSd23Q==
> -----END RSA PRIVATE KEY-----
>```

#### Paso 2 - Generar CSR con datos ya existentes
Se puede generar una solicitud CSR para un nuevo certificado en base a la información del certificado a vencerse conel siguiente comando.

```bash
openssl x509 -x509toreq -in /opt/ssl/nginx.crt -out segip2022.csr -signkey /opt/ssl/nginx.key
```

Ejemplo de salida:
>```bash
>Getting request Private Key 
>Generating certificate request
>```

#### Paso 3 - Verificar la integridad del CSR generado
Se generó una nueva solicitud CSR, para verificar la integridad el mismo, ejecute:
```bash
openssl req -text -noout -verify -in segip2022.csr
```
Ejemplo de salida
>```bash
>verify OK 
>Certificate Request:
>    Data:
>        Version: 1 (0x0)
>        Subject: C = BO, L = Santa Cruz de la Sierra, O = Servicio General de Identificacion Personal, OU = UNIDAD DE IMPLEMENTACION Y EXPLOTACION DE APLICACIONES, CN = *.segip.gob.bo
>        Subject Public Key Info:
>            Public Key Algorithm: rsaEncryption
>                RSA Public-Key: (2048 bit)
>                Modulus:
>                    00:d7:db:af:0b:b9:b5:f9:08:b0:eb:21:25:d5:5a:
>                    55:af:a5:4d:8f:03:13:48:d8:ef:71:16:0d:2e:21:
>                    c3:cc:9c:4b:0b:78:d7:e5:85:49:ce:d8:38:54:d0:
>                    . . .
>```

### Generar un CSR para una llave privada ya existente y completar la información _Subject_ en forma manual.
#### Paso 1 - Verificar la integridad de la llave privada existente.
```bash
openssl rsa -in /opt/ssl/nginx.key -check
```
Ejemplo de salida
>```bash
>RSA key ok 
>writing RSA key 
>-----BEGIN RSA PRIVATE KEY----- 
>MIIEpAIBAAKCAQEA19uvC7m1+Qiw6yEl1VpVr6VNjwMTSNjvcRYNLiHDzJxLC3jX
>5YVJztg4VNCrfPHMJm8a+Who56oJcarczTh6Q/SuTh62kaIUFviKbybWDnD0wLvj
>BVeGNXAeQyQaaD8x7vhquaNaAuwYcBCrJPWNLm4lz/BT5UEPi90+MZ8Rt314mFjR
> ...
>xvCEeem8gPUm5ASLq9z1vLQLu4W7wBa9h2vay2pcu3z9kRkOaSd23Q==
> -----END RSA PRIVATE KEY-----
>```

#### Paso 2 - Generar un nuevo CSR con la clave privada existente.
```bash
openssl req -out Segip2022.csr -key /opt/ssl/nginx.key -new
```
#### Paso 3 - Completar los campos solicitados para finalizar con la creación del archivo

Ejemplo de salida

 
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default
    value,
    If you enter '.', the field will be left blank
    -----
    Country Name (2 letter code) [AU]: 
    . . .

#### Paso 4 - Verificar la integridad del CSR generado
Se generó una nueva solicitud CSR, para verificar el mismo ejecute:
```bash
openssl req -text -noout -verify -in Segip2022.csr
```
Ejemplo de salida
>```bash
>verify OK 
>Certificate Request:
>    Data:
>        Version: 1 (0x0)
>        Subject: C = BO, L = Santa Cruz de la Sierra, O = Servicio General de Identificacion Personal, OU = UNIDAD DE IMPLEMENTACION Y EXPLOTACION DE APLICACIONES, CN = *.segip.gob.bo
>        Subject Public Key Info:
>            Public Key Algorithm: rsaEncryption
>                RSA Public-Key: (2048 bit)
>                Modulus:
>                    00:d7:db:af:0b:b9:b5:f9:08:b0:eb:21:25:d5:5a:
>                    55:af:a5:4d:8f:03:13:48:d8:ef:71:16:0d:2e:21:
>                    c3:cc:9c:4b:0b:78:d7:e5:85:49:ce:d8:38:54:d0:
>                    . . .
>```

## GENERAR UN CERTIFICADO DIGITAL A PARTIR DEL CSR GENERADO
El archivo CSR (Solicitud de Certificado Digital) debe ser enviado a una entidad de Certificación Digital para su correspondiente emisión.

Cada entidad Certificadora tiene sus procedimientos para la emisión de los mismos, para referencia consulte la documentación proporcionada en sus paginas oficiales, para referencia se puede consultar la documentación de (DigiCert, Inc, 2022) <https://docs.digicert.com/>

Una vez obtenido el certificado digital puede proceder a instalarlo en el servidor

## Actualizar el certificado en los archivos de configuración del servidor
### Respaldo de seguridad al certificado antiguo y clave privada
#### Paso 1 - Encontrar la ruta del antiguo certificado y clave privada

Con un editor de texto, abrir el archivo de configuración de algun sitio montado en el servidor nginx.

```bash
cat /etc/nginx/sites-enabled/<nombre_sitio>.conf
```
Ejemplo de salida.


   server {
        ...
   }
   server {
        listen 443 ssl;
        server_name <nombre_sitio>.segip.gob.bo;
        
        ...
        ssl_certificate                 /opt/ssl/nginx.crt;
        ssl_certificate_key             /opt/ssl/nginx.key;
        ssl_protocols                   TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers       on;
        ssl_ciphers                     'EECDH+AESGCM:EDH+AESGCM:AES256<+EECDH:AES256+EDH';
        ssl_session_cache               shared:SSL:10m;ssl_session_timeout             10m;

        ...
        location / {
           proxy_pass      http://10.0.4.196:57006;
           include         /etc/nginx/proxy_params;
        }
    }                               


Prestar atención a las rutas de:

* ssl_certificate                 /opt/ssl/nginx.crt;
* ssl_certificate_key             /opt/ssl/nginx.key;

#### Paso 2 - Renombrar los archivos antiguos .crt y .key
```bash
mv /opt/ssl/nginx.crt /opt/ssl/oldCerts/nginxOld20220523.crt
mv /opt/ssl/nginx.key /opt/ssl/oldCerts/nginxOld20220523.key
```
### Instalación del Nuevo Certificado Digital y Clave Privada
#### Paso 1 - Copiar nueva clave privada a la ruta _/opt/ssl_
Copiar la clave privada con la que se generó la solicitud CSR a la ubicación de la antigua la clave.
```bash
cp segip2022.key /opt/ssl/nginx.key
```

#### Paso 2 - Copiar nuevo certificado digital a la ruta _/opt/ssl_
Renombrar el nuevo certificado PEM a CRT y copiarlo en el directorio original del antiguo certificado.

```bash
cp segip2022.pem /opt/ssl/nginx.crt
```

#### Paso 3 - Reiniciar el servidor

Actualizar las configuraciones de los sitios del servidor http (si corresponde) y reiniciar el servidor.

```bash
sudo /etc/init.d/nginx restart
```

Una vez finalizados los pasos verificar en los sitios afectados que el certificado se actualizo correctamente. 

![](images/inscert001.png)

![](images/inscert002.png)



> **Ultima Modificacón.-** 26/05/2022 @JhonnyMonrroy

